module.exports = [
    {block: 'teaser', content: [
        {elem: 'icon', cls: 'text-primary', content: [
            {block: 'fic', icon: 'mail'},
        ]},
        {elem: 'body', content: [
            {elem: 'mail', content: 'info@eobuv.ru'},
        ]},
    ]},
];
