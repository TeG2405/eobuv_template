module.exports = [
    {block: 'teaser', content: [
        {elem: 'icon', cls: 'text-primary', content: [
            {block: 'fic', icon: 'phone'},
        ]},
        {elem: 'body', content: [
            {elem: 'phone', content: '8 800 100-03-24'},
            {elem: 'substring', content: 'Звонок по России бесплатный'},
        ]},
    ]},
];
