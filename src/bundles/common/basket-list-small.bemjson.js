const items = [
  {
    image: 'http://placehold.it/50x50',
    name: 'Сапоги COVER',
    size: '39',
    code: 'T12743-223',
    price: '4 490 руб.',
    priceOld: '5 490 руб.',
  },
  {
    image: 'http://placehold.it/50x50',
    name: 'Сапоги замшевые зимние COVER',
    size: '49',
    code: 'T12743-223',
    price: '2 990 руб.',
  },
  {
    image: 'http://placehold.it/50x50',
    name: 'Сапоги COVER',
    size: '39',
    code: 'T12743-223',
    price: '4 490 руб.',
    priceOld: '5 490 руб.',
  },
  {
    image: 'http://placehold.it/50x50',
    name: 'Сапоги замшевые зимние COVER',
    size: '49',
    code: 'T12743-223',
    price: '2 990 руб.',
  },
  {
    image: 'http://placehold.it/50x50',
    name: 'Сапоги COVER',
    size: '39',
    code: 'T12743-223',
    price: '4 490 руб.',
    priceOld: '5 490 руб.',
  },
  {
    image: 'http://placehold.it/50x50',
    name: 'Сапоги замшевые зимние COVER',
    size: '49',
    code: 'T12743-223',
    price: '2 990 руб.',
  },
];

module.exports = [
  {block: 'basket-list-small', content: [
    items.map((item)=>{
      return [
        {elem: 'li', content: [
          {elem: 'item', content: [
            item.image &&
            {elem: 'image', content: [
              {block: 'img', src: item.image},
            ]},
            {elem: 'body', content: [
              {elem: 'title', content: [
                {elem: 'text', content: item.name},
                {elem: 'badge', content: item.size},
              ]},
              {elem: 'code', content: item.code},
              {elem: 'price', content: [
                {block: 'price', cls: 'mr-3', content: item.price},
                item.priceOld &&
                {block: 'price', mods: {old: true}, content: item.priceOld},
              ]},
              {elem: 'control'},
            ]},
          ]},
        ]},
      ];
    }),
  ]},
];
