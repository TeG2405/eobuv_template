var items = [
    'http://placehold.it/80x40/00FF00',
    'http://placehold.it/120x50/00FFF0',
    'http://placehold.it/90x45/00F000',
    'http://placehold.it/50x50/00F00F',
    'http://placehold.it/130x100/F00F00',
    'http://placehold.it/80x40/000FF0',
    'http://placehold.it/120x50/0FF000',
    'http://placehold.it/90x45/FF0000',
    'http://placehold.it/50x50/0000FF',
    'http://placehold.it/130x100/FF0000',
];
module.exports = [
    {block: 'swiper', mods: {brand: true}, content: [
        {elem: 'container', content: [
            {elem: 'wrapper', cls: 'align-items-center', content: [
                items.map((src)=>{
                    return [
                        {elem: 'slide', content: [
                            {block: 'a', content: [
                                {block: 'img', src: src || 'http://placehold.it/50x50'},
                            ]},
                        ]},
                    ];
                }),
            ]},
            {elem: 'pagination'},
            {elem: 'button', elemMods: {prev: true}, cls: 'fic fic-angle-left'},
            {elem: 'button', elemMods: {next: true}, cls: 'fic fic-angle-right'},
        ]},
    ]},
];
