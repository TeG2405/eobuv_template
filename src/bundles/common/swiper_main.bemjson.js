var items = [
    'upload/swiper_main_1.jpg',
    'http://placehold.it/1110x500',
    'http://placehold.it/1110x500',
];
module.exports = [
    {block: 'swiper', mods: {main: true}, content: [
        {elem: 'container', content: [
            {elem: 'wrapper', content: [
                items.map((src)=>{
                    return [
                        {elem: 'slide', cls: 'container', content: [
                            {block: 'a', content: [
                                {block: 'img', cls: 'img-fluid rounded-bottom', src: src || 'http://placehold.it/1110x500/'},
                            ]},
                        ]},
                    ];
                }),
            ]},
            {elem: 'pagination'},
            {elem: 'button', elemMods: {prev: true}, cls: 'bg-white fic fic-angle-left'},
            {elem: 'button', elemMods: {next: true}, cls: 'bg-white fic fic-angle-right'},
        ]},
    ]},
];
