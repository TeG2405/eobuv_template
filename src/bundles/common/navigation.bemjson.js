module.exports = [
  {block: 'navigation', content: [
    {elem: 'inner', content: [
      {elem: 'item', content: [
        {elem: 'link', content: 'Женская'},
        {elem: 'dropdown', content: [
          {elem: 'back', content: 'Женская'},
          {elem: 'container', content: [
            {elem: 'row', content: [
              {elem: 'col', elemMods: {half: true}, content: [
                {elem: 'title', content: 'Категории'},
                {elem: 'list', elemMods: {split: '2'}, content: [
                  'Балетки',
                  'Босоножки',
                  'Ботильоны',
                  'Ботинки',
                  'Домашняя обувь',
                  'Дутики и луноходы',
                  'Кроссовки и кеды',
                  'Мокасины и топсайдеры',
                  'Обувь с увеличенной полнотой',
                  'Домашняя обувь',
                  'Дутики и луноходы',
                  'Кроссовки и кеды',
                  'Мокасины и топсайдеры',
                  'Обувь с увеличенной полнотой',
                ]},
              ]},
              {elem: 'col', content: [
                {elem: 'title', content: 'Коллекция'},
                {elem: 'list', content: [
                  'Зима 2018',
                  'Весна 2018',
                  'Лето 2018',
                  'Осень 2018',
                ]},
              ]},
              {elem: 'col', elemMods: {grow: '0'}, content: [
                {block: 'thumbnail', content: [
                  {block: 'img', cls: 'img-fluid', src: 'http://placehold.it/200x300'},
                ]},
              ]},
            ]},
          ]},
        ]},
      ]},
      {elem: 'item', content: [
        {elem: 'link', content: 'Мужская'},
        {elem: 'dropdown', content: [
          {elem: 'back', content: 'Мужская'},
          {elem: 'container', content: [
            {elem: 'row', content: [
              {elem: 'col', elemMods: {half: true}, content: [
                {elem: 'title', content: 'Категории'},
                {elem: 'list', elemMods: {split: '2'}, content: [
                  'Летние туфли',
                  'Туфли',
                  'Ботинки',
                  'Мокасины',
                  'Спорт.туфли',
                  'Сандалии',
                  'Тапочки',
                  'Полуботинки',
                  'Кроссовки',
                  'Сланцы',
                ]},
              ]},
              {elem: 'col', content: [
                {elem: 'title', content: 'Коллекция'},
                {elem: 'list', content: [
                  'Зима 2018',
                  'Весна 2018',
                  'Лето 2018',
                  'Осень 2018',
                ]},
              ]},
              {elem: 'col', elemMods: {grow: '0'}, content: [
                {block: 'thumbnail', content: [
                  {block: 'img', cls: 'img-fluid', src: 'http://placehold.it/200x300'},
                ]},
              ]},
            ]},
          ]},
        ]},
      ]},
      {elem: 'item', content: [
        {elem: 'link', content: 'Аксессуары'},
        {elem: 'dropdown', content: [
          {elem: 'back', content: 'Аксессуары'},
          {elem: 'container', content: [
            {elem: 'row', content: [
              {elem: 'col', elemMods: {half: true}, content: [
                {elem: 'title', content: 'Категории'},
                {elem: 'list', content: [
                  'Зонты',
                  'Кошелёк',
                  'Обложки',
                  'Обувная Косметика',
                  'Ремни',
                  'Сумки',
                  'Сушилки',
                ]},
              ]},
              {elem: 'col', content: [
                {elem: 'title', content: 'Коллекция'},
                {elem: 'list', content: [
                  'Зима 2018',
                  'Весна 2018',
                  'Лето 2018',
                  'Осень 2018',
                ]},
              ]},
              {elem: 'col', elemMods: {grow: '0'}, content: [
                {block: 'thumbnail', content: [
                  {block: 'img', cls: 'img-fluid', src: 'http://placehold.it/200x300'},
                ]},
              ]},
            ]},
          ]},
        ]},
      ]},
      {elem: 'item', content: [
        {elem: 'link', content: 'Бренды'},
        {elem: 'dropdown', content: [
          {elem: 'back', content: 'Бренды'},
          {elem: 'container', content: [
            {elem: 'row', content: [
              {elem: 'col', content: [
                {elem: 'list', elemMods: {split: '4'}, content: [
                  'Alice + Olivia',
                  'Armani Exchange',
                  'Baldinini',
                  'Casadei',
                  'Coach',
                  'Coccinelle',
                  'DKNY',
                  'Dolce&Gabbana',
                  'Emporio Armani',
                  'Furla',
                  'Hugo Hugo Boss',
                  'Iceberg',
                  'Karl Lagerfeld',
                  'Lauren Ralph Lauren',
                  'Liu Jo',
                  'Love Moschino',
                  'MAX&Co',
                  'McQ Alexander McQueen',
                  'Michael Michael Kors',
                  'Nando Muzi',
                  'Stuart Weitzman',
                  'Tory Burch',
                  'Trussardi Jeans',
                  'Twin-Set Simona Barbieri',
                  'Weekend Max Mara',
                ]},
              ]},
            ]},
          ]},
        ]},
      ]},
      {elem: 'item', content: [
        {elem: 'link', content: 'Акции'},
        {elem: 'dropdown', content: [
          {elem: 'back', content: 'Акции'},
          {elem: 'container', content: [
            {elem: 'row', content: [
              {elem: 'col', elemMods: {grow: '0'}, content: require('./card_action.bemjson')},
              {elem: 'col', elemMods: {grow: '0'}, content: require('./card_action.bemjson')},
              {elem: 'col', elemMods: {grow: '0'}, content: require('./card_action.bemjson')},
            ]},
          ]},
        ]},
      ]},
    ]},
  ]},
];
