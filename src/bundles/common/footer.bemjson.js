module.exports = [
  {block: 'footer', content: [
    {elem: 'container', mix: {block: 'container'}, content: [
      {elem: 'inner', content: [
        {elem: 'row', content: [
          {elem: 'col', cls: 'd-none d-xl-block', content: [
            {elem: 'title', cls: 'text-uppercase collapsed', content: 'Каталог'},
            {elem: 'collapse', content: [
              {elem: 'list', content: [
                'Женская обувь',
                'Мужская обувь',
                'Аксессуары',
                'Бренды',
              ]},
            ]},
          ]},
          {elem: 'col', content: [
            {elem: 'title', cls: 'text-uppercase collapsed', content: 'Компания'},
            {elem: 'collapse', content: [
              {elem: 'list', content: [
                'О компании',
                'Магазины',
                'Вакансии',
                'Благотворительность',
                'Отзывы работников',
                'Отзывы покупателей',
              ]},
            ]},
          ]},
          {elem: 'col', content: [
            {elem: 'title', cls: 'text-uppercase collapsed', content: 'Клиентам'},
            {elem: 'collapse', content: [
              {elem: 'list', content: [
                'Доставка',
                'Возврат',
                'Вопрос/Ответ',
                'Сотрудничество',
                'Акции',
                'Новости',
              ]},
            ]},
          ]},
          {elem: 'col', content: [
            {elem: 'title', cls: 'text-uppercase collapsed', content: 'Cвязаться с нами'},
            {elem: 'collapse', content: [
              {elem: 'group', content: [
                require('./teaser_phone.bemjson'),
              ]},
              {elem: 'group', content: [
                require('./teaser_mail.bemjson'),
              ]},
              {elem: 'group', content: [
                {block: 'btn', cls: 'btn-primary px-5', content: 'Заказать звонок'},
              ]},
            ]},
          ]},
          {elem: 'col', elemMods: {social: true}, content: [
            {elem: 'logo', content: [
              require('./logo.bemjson'),
            ]},
            {elem: 'title', content: 'ЕвроОбувь в соц.сетях'},
            {elem: 'collapse', cls: 'show', content: [
              {block: 'social', content: [
                {elem: 'list', content: [
                  'vk',
                  'facebook',
                  'odnoklassniki',
                  'instagram',
                  'google-plus',
                ]},
              ]},
            ]},
          ]},
        ]},
      ]},
    ]},
    {block: 'copyright', content: [
      {mix: {block: 'container'}, content: [
        {elem: 'inner', content: [
          {elem: 'company', content: '© 2012-2018 «ЕвроОбувь»'},
          {elem: 'developer', content: [
            {block: 'fic', cls: 'pr-2 fic-iv'},
            'Разработка сайта',
          ]},
        ]},
      ]},
    ]},
  ]},
];
