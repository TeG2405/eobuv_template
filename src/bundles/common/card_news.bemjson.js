module.exports = [
    {block: 'card', mods: {news: true}, cls: 'border-0', tag: 'a', attrs: {href: '#'}, content: [
        {elem: 'image', content: [
            {block: 'img', cls: 'card-img-top rounded', src: 'http://placehold.it/260x200'},
        ]},
        {block: 'h', size: '5', content: 'Новый магазин ЕвроОбувь открылся в городе Нальчике'},
        {content: 'С декабря жителям Нальчика совершать покупки в ЕвроОбувь вдвойне удобнее! '},
    ]},
];
