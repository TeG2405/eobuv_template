module.exports = [
  {block: 'header', content: [
    {elem: 'top', content: [
      {elem: 'container', mix: {block: 'container'}, content: [
        {elem: 'row', content: [
          {elem: 'col', content: [
            {block: 'teaser', content: [
              {elem: 'icon', cls: 'text-primary', content: [
                {block: 'fic', icon: 'map-marker'},
              ]},
              {elem: 'body', content: [
                {elem: 'title', content: 'Местоположение'},
                {block: 'a', mods: {underline: 'dashed'}, content: 'Волгоград'},
              ]},
            ]},
          ]},
          {elem: 'col', cls: 'd-none d-sm-block', content: [
            require('./teaser_phone.bemjson'),
          ]},
          {elem: 'col', cls: 'd-none d-lg-block', content: [
            {block: 'teaser', content: [
              {elem: 'icon', cls: 'text-primary', content: [
                {block: 'fic', icon: 'clock'},
              ]},
              {elem: 'body', content: [
                {elem: 'title', content: 'Время работы: 9:00 - 18:00'},
                {elem: 'substring', content: 'Без перерыва и выходных'},
              ]},
            ]},
          ]},
          {elem: 'col', cls: 'd-none d-xl-block', content: [
            {block: 'teaser', tag: 'a', content: [
              {elem: 'icon', cls: 'text-primary', content: [
                {block: 'fic', icon: 'phone-call-circle'},
              ]},
              {elem: 'body', content: [
                'Заказать <br/> звонок',
              ]},
            ]},
          ]},
          {elem: 'col', content: [
            {block: 'teaser', tag: 'a', content: [
              {elem: 'icon', cls: 'text-primary', content: [
                {block: 'fic', icon: 'lock'},
              ]},
              {elem: 'body', cls: 'text-dark', content: [
                'Войти',
              ]},
            ]},
          ]},
        ]},
      ]},
    ]},
    {elem: 'sticky', content: [
      {elem: 'inner', content: [
        {elem: 'container', mix: {block: 'container'}, content: [
          {elem: 'row', content: [
            {elem: 'col', content: [
              require('./logo.bemjson.js'),
            ]},
            {elem: 'col', elemMods: {navigation: true}, content: [
              {elem: 'row', elemMods: {teaser: true}, content: [
                {elem: 'col', content: [
                  require('./logo.bemjson.js'),
                ]},
              ]},
              require('./navigation.bemjson.js'),
              {elem: 'row', elemMods: {teaser: true}, cls: 'd-lg-none', content: [
                {elem: 'col', cls: 'd-sm-none', content: [
                  require('./teaser_phone.bemjson'),
                ]},
                {elem: 'col', cls: 'd-lg-none', content: [
                  {block: 'teaser', content: [
                    {elem: 'icon', cls: 'text-primary', content: [
                      {block: 'fic', icon: 'clock'},
                    ]},
                    {elem: 'body', content: [
                      {elem: 'title', content: 'Время работы: 9:00 - 18:00'},
                      {elem: 'substring', content: 'Без перерыва и выходных'},
                    ]},
                  ]},
                ]},
              ]},
            ]},
            {elem: 'col', elemMods: {toolbar: true}, content: [
              {block: 'toolbar', content: [
                {elem: 'inner', content: [
                  {elem: 'item', content: [
                    {elem: 'link', tag: 'span', attrs: {'data-toggle': 'show', 'data-target': '.header__sticky .search'}, content: [
                      {elem: 'icon', content: [
                        {block: 'fic', icon: 'search'},
                      ]},
                    ]},
                  ]},
                  {elem: 'item', content: [
                    {elem: 'dropdown', attrs: {id: 'FAVORITES'}, content: [
                      {block: 'basket-small', content: [
                        {elem: 'back', content: 'Избранные товары', attrs: {'data-toggle': 'show', 'data-target': '#FAVORITES'}},
                        {elem: 'body', content: [
                          require('./basket-list-small.bemjson.js'),
                        ]},
                        {elem: 'footer', content: [
                          {block: 'btn', cls: 'btn-primary btn-block', content: 'Перейти в избранные'},
                        ]},
                      ]},
                    ]},
                    {elem: 'link', attrs: {'data-toggle': 'show', 'href': '#FAVORITES'}, content: [
                      {elem: 'icon', content: [
                        {block: 'fic', icon: 'heart-o'},
                      ]},
                      {elem: 'badge', content: 6},
                    ]},
                  ]},
                  {elem: 'item', content: [
                    {elem: 'dropdown', attrs: {id: 'BASKET_SMALL'}, content: [
                      {block: 'basket-small', content: [
                        {elem: 'back', content: 'Товары в корзине', attrs: {'data-toggle': 'show', 'data-target': '#BASKET_SMALL'}},
                        {elem: 'body', content: [
                          require('./basket-list-small.bemjson.js'),
                        ]},
                        {elem: 'footer', content: [
                          {elem: 'total', content: 'Итого: 7 480 руб.'},
                          {block: 'btn', cls: 'btn-primary btn-block', content: 'Перейти в корзину'},
                        ]},
                      ]},
                    ]},
                    {elem: 'link', attrs: {'data-toggle': 'show', 'href': '#BASKET_SMALL'}, content: [
                      {elem: 'icon', content: [
                        {block: 'fic', icon: 'shopping-bag'},
                      ]},
                      {elem: 'badge', content: 3},
                    ]},
                  ]},
                  {elem: 'item', elemMods: {only: 'touch'}, content: [
                    {elem: 'link', tag: 'span', attrs: {'data-toggle': 'show', 'data-target': '.header__col_navigation'}, content: [
                      {elem: 'icon', content: [
                        {block: 'fa', icon: 'bars'},
                      ]},
                    ]},
                  ]},
                ]},
              ]},
            ]},
          ]},
        ]},
        {block: 'search', cls: 'fade', content: [
          {elem: 'container', mix: {block: 'container'}, content: [
            {elem: 'inner', content: [
              {elem: 'label', content: [
                {elem: 'input'},
              ]},
              {elem: 'btn', content: [
                {block: 'fic', icon: 'search'},
              ]},
            ]},
          ]},
        ]},
      ]},
    ]},
  ]},
];
