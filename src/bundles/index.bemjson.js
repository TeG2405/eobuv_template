module.exports = {
  block: 'page',
  title: 'Пустая странциа',
  content: [
    require('./common/header.bemjson.js'),
    {block: 'main', content: [
      require('./common/swiper_main.bemjson'),
      {block: 'bg-light', cls: 'py-5 my-5', content: [
        {mix: {block: 'container'}, content: [

        ]},
      ]},
      {mix: {block: 'container'}, content: [
        // Бренды:
        {block: 'd-flex', cls: 'h1 justify-content-between align-items-center', content: [
          {block: 'h', size: '2', cls: 'my-0', content: 'Женская и мужская обувь'},
          {block: 'btn', cls: 'btn-outline-primary btn-sm px-4', content: 'Все бренды (250)'},
        ]},
        {block: 'row', content: [
          {block: 'col', cls: 'mb-4', content: [
            require('./common/swiper_brand.bemjson'),
          ]},
        ]},
        // Новости:
        {block: 'd-flex', cls: 'h1 justify-content-between align-items-center', content: [
          {block: 'h', size: '2', cls: 'my-0', content: 'Новости'},
          {block: 'btn', cls: 'btn-outline-primary btn-sm px-4', content: 'Все новости'},
        ]},
        {block: 'row', content: [
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_news.bemjson')},
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_news.bemjson')},
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_news.bemjson')},
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_news.bemjson')},
        ]},
        // Акции:
        {block: 'd-flex', cls: 'h1 justify-content-between align-items-center', content: [
          {block: 'h', size: '2', cls: 'my-0', content: 'Акции'},
          {block: 'btn', cls: 'btn-outline-primary btn-sm px-4', content: 'Все акции'},
        ]},
        {block: 'row', content: [
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_action.bemjson')},
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_action.bemjson')},
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_action.bemjson')},
          {block: 'col-12', cls: 'col-sm-6 col-xl-3 mb-5', content: require('./common/card_action.bemjson')},
        ]},
        // Коллапс с описанием страницы:
        {block: 'd-flex', cls: 'h1 justify-content-between align-items-center', content: [
          {block: 'h', size: '2', cls: 'my-0', content: 'Интернет-магазин ЕвроОбувь'},
        ]},
        {tag: 'p', content: 'Интернет-магазин обуви и аксессуаров "ЕвроОбувь" предлагает товары только лучших европейских производителей, что гарантирует высочайшее качество и надежность товаров. На всю нашу продукцию мы предоставляем гарантию.'},
        {tag: 'p', content: 'В нашем магазине в большом ассортименте мужская и женская обувь, таких мировых брендов: Tamaris(Тамарис), Caprice (Каприз), s.Oliver (Оливер), Rieker (Рикер), CV Cover (Ковер), CavaLetto (Кавалето), El Tempo, Marco Tozzi, Bruno Renzoni, Federica Rodari, Alpina, Olivia.'},
        {tag: 'p', content: 'Наши магазины обуви открыты в таких городах как: Ставрополь, Пятигорск, Ессентуки, Кисловодск, Минеральные воды, Невинномысск, Владикавказ, Нальчик, Севастополь, Симферополь, Волжский, Волгоград, Шахты, Черкесск, Таганрог, Ростов. '},
        {block: 'a', mods: {underline: 'dashed'}, cls: 'text-muted', content: 'Читать полностью'},
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
