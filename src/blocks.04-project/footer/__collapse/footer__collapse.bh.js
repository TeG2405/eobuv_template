module.exports = function(bh) {
    bh.match('footer__collapse', function(ctx, json) {
        ctx
            .cls('collapse')
            .attrs({
                id: ctx.tParam('ID') || ctx.generateId(),
            });
    });
};
