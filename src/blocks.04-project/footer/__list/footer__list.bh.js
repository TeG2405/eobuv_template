module.exports = function(bh) {
    bh.match('footer__list', function(ctx, json) {
        ctx.tag('ul').content([
            ctx.content().map((item)=>{
                return ctx.isSimple(item) ? {elem: 'li', content: {elem: 'link', content: item}} : item;
            }),
        ], true);
    });
};
