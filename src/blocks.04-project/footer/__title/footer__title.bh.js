module.exports = function(bh) {
    bh.match('footer__title', function(ctx, json) {
        ctx
            .tag('a')
            .attrs({
                'href': `#${ctx.tParam('ID') || ctx.generateId()}`,
                'data-toggle': 'collapse',
                'aria-expanded': 'false',
                'role': 'button',
            });
    });
};
