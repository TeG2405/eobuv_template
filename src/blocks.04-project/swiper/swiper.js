import Swiper from 'swiper/dist/js/swiper.min';


$(window).on('load', function() {
    // Слайдер на главной странице
    new Swiper('.swiper_main .swiper-container', {
        centeredSlides: true,
        slidesPerView: 'auto',
        loop: true,
        spaceBetween: 0,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper__button_next',
            prevEl: '.swiper__button_prev',
        },
    });

    // Слайдер на главной странице бренды
    new Swiper('.swiper_brand .swiper-container', {
        slidesPerView: 'auto',
        spaceBetween: 0,
        centeredSlides: true,
        loop: true,
        navigation: {
            nextEl: '.swiper__button_next',
            prevEl: '.swiper__button_prev',
        },
    });
});
