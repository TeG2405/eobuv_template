module.exports = function(bh) {
    bh.match('navigation__back', function(ctx, json) {
        ctx.content([
            ctx.content(),
            {block: 'fa', icon: 'times'},
        ], true);
    });
};
