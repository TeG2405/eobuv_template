module.exports = function(bh) {
    bh.match('search__input', function(ctx, json) {
        ctx
            .tag('input')
            .attrs({
                type: 'text',
                placeholder: 'Поиск по сайту',
                id: ctx.tParam('ID')
            });
    });
};
