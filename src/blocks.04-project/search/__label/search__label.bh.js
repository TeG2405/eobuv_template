module.exports = function(bh) {
    bh.match('search__label', function(ctx, json) {
        ctx
            .tag('label')
            .attr('for', ctx.tParam('ID'));
    });
};
