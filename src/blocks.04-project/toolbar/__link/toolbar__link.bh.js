module.exports = function(bh) {
    bh.match('toolbar__link', function(ctx, json) {
        ctx.tag('a');
        if (ctx.tag() === 'a') {
            ctx.attr('href', json.href || '#');
        }
    });
};
