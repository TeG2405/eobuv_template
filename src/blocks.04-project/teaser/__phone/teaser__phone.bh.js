module.exports = function(bh) {
    bh.match('teaser__phone', function(ctx, json) {
        ctx.tag('a').attrs({
            href: `tel:${ctx.content().replace(/[^0-9]*/g, '')}`,
        });
    });
};
