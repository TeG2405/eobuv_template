module.exports = function(bh) {
    bh.match('teaser', function(ctx, json) {
        if (ctx.tag() == 'a') {
            ctx.attr('href', '#');
        }
    });
};
