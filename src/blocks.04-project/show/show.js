(($)=>{
    const ClassName = {
        OPEN: 'backdrop-open',
        SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
        BACKDROP: 'backdrop',
        FADE: 'fade',
        SHOW: 'show',
    };
    class Show {
        constructor() {
            this.$stack = $();
            this.$body = $('body');
            this.$header = $('.header');
            this.$headerInner = $('.header__inner');
            this.$headerColToolbar = $('.header__col_toolbar');
            this.$search = this.$header.find('.search');
            this.scrollWidth = Show.getScrollWidth();
            this._backdrop = null;
        }
        toggle(elem) {
            let $target = Show.getTarget(elem);
            this.$stack
                .not($target)
                .removeClass('show');
            this.$stack = $target.toggleClass('show').filter('.show');
            this._checkBackdrop();
        }
        clear(event) {
            if (this.$stack.has(event.target).length) return;
            this.$stack = this.$stack.removeClass('show').filter('.show');
            this._checkBackdrop();
        }
        _checkBackdrop() {
            if (this.$stack.length) {
                if (this.$body.hasClass(ClassName.OPEN)) return;
                this._showBackdrop();
            } else {
                if (!this.$body.hasClass(ClassName.OPEN)) return;
                this._hideBackdrop();
            }
        }
        _showBackdrop() {
            let isBodyOverflowing = Show.isBodyOverflowing();
            this.$body.addClass(ClassName.OPEN);
            if (isBodyOverflowing) {
                this.$body.css({
                    paddingRight: `${this.scrollWidth}px`,
                });
                this.$header.css({
                    paddingRight: `${this.scrollWidth}px`,
                    marginRight: `-${this.scrollWidth}px`,
                });
                this.$headerInner.css('position') == 'fixed' && this.$headerInner.css({
                    paddingRight: `${this.scrollWidth}px`,
                    marginRight: `-${this.scrollWidth}px`,
                });
                this.$headerInner.css('position') == 'fixed' && this.$search.css({
                    paddingRight: `${this.scrollWidth}px`,
                    marginRight: `-${this.scrollWidth}px`,
                });
                this.$headerColToolbar.css('position') == 'fixed' && this.$headerColToolbar.css({
                    paddingRight: `${this.scrollWidth}px`,
                    marginRight: `-${this.scrollWidth}px`,
                });
            }
            this._addBackdrop();
        }
        _hideBackdrop() {
            this.$body.removeClass(ClassName.OPEN);
            this.$body.css({
                paddingRight: '',
            });
            this.$headerColToolbar.css({
                paddingRight: '',
                marginRight: '',
            });
            this.$header.css({
                paddingRight: '',
                marginRight: '',
            });
            this.$headerInner.css({
                paddingRight: '',
                marginRight: '',
            });
            this.$search.css({
                paddingRight: '',
                marginRight: '',
            });
            this._removeBackdrop();
        }
        _removeBackdrop() {
            if (this._backdrop) {
                $(this._backdrop).removeClass(ClassName.SHOW);
            }
        }
        _addBackdrop() {
            if (!this._backdrop) {
                this._backdrop = document.createElement('div');
                this._backdrop.className = ClassName.BACKDROP;
                $(this._backdrop)
                    .addClass(ClassName.FADE)
                    .appendTo(document.body);
                this._backdrop.offsetHeight;
            }
            $(this._backdrop).addClass(ClassName.SHOW);
        }
        static getTarget(elem) {
            return $($(elem).data('target') || elem.getAttribute('href'));
        }
        static isBodyOverflowing() {
            let rect = document.body.getBoundingClientRect();
            return rect.left + rect.right < window.innerWidth;
        }
        static getScrollWidth() {
            var scrollDiv = document.createElement('div');
            scrollDiv.className = ClassName.SCROLLBAR_MEASURER;
            document.body.appendChild(scrollDiv);
            var scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
            document.body.removeChild(scrollDiv);
            return scrollbarWidth;
        }
    }
    var show = new Show();
    $(document)
        .on('click.block.show', '[data-toggle="show"]', function(e) {
            show.toggle(e.currentTarget);
            e.preventDefault();
            e.stopPropagation();
        })
        .on('click.block.show', function(e) {
            show.clear(e);
        });
})($);
