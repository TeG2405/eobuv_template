(($)=>{
    const ClassName = {
        TARGET: '.header__inner',
        MODS: 'header__inner_fixed',
    };
    class Header {
        constructor() {
            this.$headerInner = $(ClassName.TARGET);
        }
        checkSticky() {
            if (this.$headerInner.length) {
                if (this.$headerInner.parent().offset().top > window.pageYOffset) {
                    this.$headerInner
                        .removeClass(ClassName.MODS)
                        .parent()
                        .height('auto');
                } else {
                    this.$headerInner
                        .addClass(ClassName.MODS)
                        .parent()
                        .height(this.$headerInner.outerHeight());
                }
            }
        }
    }
    var header = new Header();
    $(window)
        .on('scroll.block.header', function(e) {
            header.checkSticky();
        })
        .on('load.block.header', function(e) {
            header.checkSticky();
        });
})($);


