module.exports = function(bh) {
    bh.match('swiper__lazy', function(ctx, json) {
        ctx
            .tag('img')
            .attrs({
                'src': ctx.process({block: '1px'}),
                'data-src': json.src,
            });
        return [
            json,
            '<span class="swiper-lazy-preloader"></span>',
        ];
    });
};
