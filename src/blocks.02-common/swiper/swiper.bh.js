module.exports = function(bh) {
    bh.match('swiper', function(ctx, json) {
        bh.cbc('swiper__container', 'swiper-container');
        bh.cbc('swiper__wrapper', 'swiper-wrapper');
        bh.cbc('swiper__slide', 'swiper-slide');
        bh.cbc('swiper__pagination', 'swiper-pagination');
        // bh.cbc('swiper__button_prev', 'swiper-button-prev');
        // bh.cbc('swiper__button_next', 'swiper-button-next');
        bh.cbc('swiper__lazy', 'swiper-lazy');
        bh.cbc('swiper__preloader', 'swiper-lazy-preloader');
        bh.cbc('swiper__preloader_white', 'swiper-lazy-preloader-white');
    });
};
