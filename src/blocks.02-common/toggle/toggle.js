var Block =(($)=>{
    const DATA_TOGGLE = '[data-toggle="toggle"]';
    const TOGGLE_CONTROL_CLASS = 'toggle-show-control';
    class Block {
        constructor(element) {
            this._$block = $(element);
        }
        show() {
            this._$block
                .addClass('show')
                .addClass(TOGGLE_CONTROL_CLASS);
        }
        hide() {
            this._$block
                .removeClass('show')
                .removeClass(TOGGLE_CONTROL_CLASS);
        }
    }
    $.fn.toggle = function(config) {
        return this.each(function() {
            let $this = $(this);
            let data = $this.data('toggle');
            if (!data) {
                data = new Block(this);
                $this.data('toggle', data);
            }
            if (typeof config === 'string') {
                data[config]();
            }
        });
    };
    $.fn.toggle.Constructor = Block;
    $(document)
        .on('click.block.toggle', DATA_TOGGLE, function() {
            let $this = $(this);
            if ($this.data('target')) {
                $($this.data('target')).toggle('show');
            } else {
                $($this.attr('href')).toggle('show');
            }
        })
        .on('click.block.toggle', `${DATA_TOGGLE}, .${TOGGLE_CONTROL_CLASS}`, function(e) {
            e.stopPropagation();
        })
        .on('click.block.toggle', function() {
            $(`.${TOGGLE_CONTROL_CLASS}`).toggle('hide');
        });
    return Block;
})($);
export default Block;
