module.exports = function(bh) {
    bh.match('fic', function(ctx, json) {
        ctx
            .tag('i')
            .attrs({
                'aria-hidden': 'true',
            })
            .cls(json.icon && 'fic-'+json.icon);
    });
};
