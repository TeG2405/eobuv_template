module.exports = function(bh) {
    bh.match('fa', function(ctx, json) {
        ctx
            .tag('i')
            .attrs({
                'aria-hidden': 'true',
            })
            .cls(json.icon && 'fa-'+json.icon);
    });
};
