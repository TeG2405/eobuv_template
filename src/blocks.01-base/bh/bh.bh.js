module.exports = function(bh) {
    bh.match('bh', function(ctx, json) {
        let blockJSON = JSON.stringify(ctx.content());
        let blockID = ctx.generateId();
        ctx
            .attr('id', blockID)
            .content([
                {tag: 'script', attrs: {async: true}, content: ctx.process(
                    `document.addEventListener("DOMContentLoaded", function(){ 
                        window.getBH(function(BH) {
                            ${blockID}.outerHTML = BH.apply(${blockJSON})
                        })
                    })`
                )},
            ], true);
    });
};